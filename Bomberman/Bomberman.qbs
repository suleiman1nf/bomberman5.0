import qbs

Project {
    minimumQbsVersion: "1.7.1"

    CppApplication {
        consoleApplication: true

        cpp.cxxLanguageVersion: "c++17"

        property path graph_lib_dir: "../Graph_lib"

        files: [
            graph_lib_dir +"/Graph.cpp",
            graph_lib_dir +"/GUI.cpp",
            graph_lib_dir +"/Window.cpp",
            "main.cpp",
        ]

        cpp.staticLibraries: [
            "fltk_images",
            "fltk",
        ]
        Properties {
            condition: qbs.targetOS.contains("windows")
            cpp.includePaths: "../fltk_win64/include"
            cpp.libraryPaths: "../fltk_win64/lib"
            cpp.cxxFlags: "-Wno-unused-parameter"
            cpp.driverLinkerFlags: "-mwindows"
            cpp.staticLibraries: outer.concat([
                "fltk_png",
                "z",
                "fltk_jpeg",
                "ole32",
                "uuid",
                "comctl32",
            ])
        }
        Properties {
            condition: qbs.targetOS.contains("linux")
            cpp.includePaths: "/usr/local/include"
            cpp.libraryPaths: "/usr/local/lib"
            cpp.staticLibraries: outer.concat([
                "png",
                "z",
                "jpeg",
                "Xrender",
                "Xcursor",
                "Xfixes",
                "Xext",
                "Xft",
                "fontconfig",
                "Xinerama",
                "pthread",
                "dl",
                "X11",
            ])
        }

        Group {     // Properties for the produced executable
            fileTagsFilter: "application"
            qbs.install: true
        }
    }
}
